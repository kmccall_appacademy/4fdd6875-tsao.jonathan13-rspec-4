class Dictionary
  attr_accessor :entries

  def initialize
    @entries = Hash.new
  end

  def add(new)
    if new.is_a?(Hash)
      @entries = @entries.merge(new)
    elsif new.is_a?(String)
      @entries[new] = nil
    end
  end

  def include?(keyword)
    keywords.include?(keyword)
  end

  def keywords
    @entries.keys.sort_by {|word| word}
  end

  def find(keyword)
    hash = Hash.new
    hash[keyword] = @entries[keyword] if include?(keyword)
    @entries.each do |k, v|
      hash[k] = v if k.include?(keyword)
    end
    hash
  end

  def printable
    result = keywords.map do |keyword|
      %Q([#{keyword}] "#{@entries[keyword]}")
    end
    result.join("\n")
  end


end
