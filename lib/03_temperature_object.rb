class Temperature

  def initialize(option_hash)
    @f = option_hash[:f]
    @c = option_hash[:c]
  end

  def in_fahrenheit
    return @f if !@f.nil?
    @f = (@c * 9 / 5.0) + 32
    @f
  end

  def in_celsius
    return @c if !@c.nil?
    @c = (@f - 32) * 5 / 9.0
    @c
  end

  def self.from_celsius(temp)
    Temperature.new({c: temp})
  end

  def self.from_fahrenheit(temp)
    Temperature.new({f: temp})
  end

end

class Celsius < Temperature

  def initialize(temp)
    @c = temp
  end

end

class Fahrenheit < Temperature

  def initialize(temp)
    @f = temp
  end

end
