class Book

  attr_accessor :title

  def initialize
  end

  def title
    result = []
    temp = @title.split(" ")
    temp.each do |word|
      if article?(word) || conjunction?(word) || preposition?(word)
        result << word
      else
        result << word.capitalize
      end
    end
    result[0].capitalize!
    result.join(" ")
  end

  private

  def article?(word)
    articles = ["a", "an", "the", "in", "of"]
    return true if articles.include?(word)
    false
  end

  def conjunction?(word)
    conjunctions = ["and", "or"]
    return true if conjunctions.include?(word)
    false
  end

  def preposition?(word)
    prepositions = ["in"]
    return true if prepositions.include?(word)
  end



end
