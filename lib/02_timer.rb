class Timer
  attr_accessor :seconds, :hours, :minutes

  def initialize
    @seconds = 0
  end

  def time_string
    make_hours
    make_minutes
    hours = padded(@hours)
    minutes = padded(@minutes)
    seconds = padded(@seconds)
    hours + ":" + minutes + ":" + seconds
  end

  private

  def make_hours
    @hours = @seconds / 3600
    @seconds = @seconds % 3600
  end

  def make_minutes
    @minutes = @seconds / 60
    @seconds = @seconds % 60
  end

  def padded(num)
    return "0#{num}" if num < 9
    "#{num}"
  end






end
